#include <iostream>
#include "Tank.h"
#include "Engine.h"

// Singleton Pattern
Engine* Engine::instance = 0;
Engine* Engine::getInstance() {
	if (!instance) {
		instance = new Engine();
	}
	return instance;
}

/*
* this function absorbs fuel and if the quantity of fuel is below 20, it fills the internal tank.
*/
void Engine::absorbFuel() {
	if (this->status) {
		if (this->internalTank->getFuelQuantity() < 20)
		{
			for (int i = 0; i < this->connectedTanks->size(); i++)
			{
				Tank* tank = this->connectedTanks->at(i).get();
				if (tank->getFuelQuantity() > 35){
					this->internalTank->fillTank(35);
					tank->absorbFuel(35);
					break;
				}
			}
		}
		this->internalTank->absorbFuel(this->fuelPerSecond);
		this->consumedFuelQuantity += this->fuelPerSecond;
	}
}

/*
* this function gives back fuel in its internal tank to the connected tank .
* @param double quantity
*/
bool Engine::giveBackFuel(double quantity) {
	while (quantity - this->internalTank->getFuelQuantity() > this->internalTank->getFuelQuantity()) {
		if (this->connectedTanks->size() > 0) {
			Tank* minFuelTank = this->connectedTanks->at(0).get();
			for (int i = 1; i < this->connectedTanks->size(); i++)
			{
				Tank* currTank = this->connectedTanks->at(i).get();
				if (currTank->getFuelQuantity() < minFuelTank->getFuelQuantity()) {
					minFuelTank = currTank;
				}
			}
			this->internalTank->absorbFuel(quantity);
			minFuelTank->fillTank(quantity);
			quantity -= this->internalTank->getFuelQuantity();
			return true;
		}
		else {
			break;
		}
	}
	return false;
}

/*
* This is the constructor for the Engine class without a parameter.
*/
Engine::Engine() {
	this->status = false;
	this->consumedFuelQuantity = 0;
	this->internalTank = new Tank(-1, 55);
	this->connectedTanks = new vector<shared_ptr<Tank>>;
}
Engine::~Engine() {
	delete this->connectedTanks;
	delete this->internalTank;
}
/*
* This function connects the fuel tanks.
* @param Tank tank
*/
bool Engine::connectFuelTank(Tank* tank) {

	this->connectedTanks->push_back((shared_ptr<Tank>)tank);
	return true;
}

/*
* This function disconnects the fuel tanks.
* @param Tank tank
*/
bool Engine::disconnectFuelTank(Tank* tank) {
	int tankId = tank->getId();
	for (int i = 0; i < this->connectedTanks->size(); i++)
	{
		Tank* currtank = this->connectedTanks->at(i).get();
		int currtankId = currtank->getId();
		if (currtankId == tankId) {
			vector<shared_ptr<Tank>>* tempconnectedTanks = new vector<shared_ptr<Tank>>();
			for (int j = 0; j < this->connectedTanks->size(); j++)
			{
				Tank* currtank2 = this->connectedTanks->at(j).get();
				if (currtankId != currtank2->getId()) {
					tempconnectedTanks->push_back((shared_ptr<Tank>)currtank2);
				}
			}
			this->connectedTanks = tempconnectedTanks;
			return true;
		}
	}
	return false;
}

/*
* This function lists the fuel tanks.
*/
void Engine::listConnectedFuelTanks() {
	if (connectedTanks->empty() == true)
	{
		cout << "there aren't any connected tanks" << endl;
	}
	else
	{
		cout << "-------  List Connected Fuel Tanks -------" << endl;
		for (int i = 0; i < connectedTanks->size(); i++)
		{
			Tank* tank = this->connectedTanks->at(i).get();
			tank->printInfo();
		}
		cout << "------------------------------------------" << endl;
	}
}

/*
* This function stops the engine.
*/
bool Engine::stopEngine() {
	this->giveBackFuel(this->internalTank->getFuelQuantity());
	this->status = false;
	return true;
}

/*
* This function returns the status of the engine.
*/
bool Engine::getStatus() const { return this->status; }

/*
* This function starts the engine.
*/
bool Engine::startEngine() {
	if (this->connectedTanks->size() > 0)
	{
		this->status = true;
		return true;
	}
	else
	{
		this->status = false;
		return false;
	}
}

/*
* This function gets the quantity of fuel in internal tank.
*/
double Engine::getTotalFuelQuantity()const {
	double totalFuelQuantity = this->internalTank->getFuelQuantity();
	for (int i = 0; i < this->connectedTanks->size(); i++)
	{
		totalFuelQuantity += this->connectedTanks->at(i).get()->getFuelQuantity();
	}
	return totalFuelQuantity;
}

/*
* This function gets the quantity of consumed fuel by the engine.
*/
double Engine::getConsumedFuelQuantity()const {
	return this->consumedFuelQuantity;
}
