#pragma once
#include <iostream>
#include <vector>
#include "Tank.h"
using namespace std;

class Engine {
private:
	static Engine* instance;
	Engine();
	const double fuelPerSecond = 5.5;
	bool status;
	Tank* internalTank;
	vector<shared_ptr<Tank>>* connectedTanks;
	double consumedFuelQuantity;
public:
	static Engine* getInstance();
	~Engine();
	void absorbFuel();
	bool giveBackFuel(double quantity);
	bool connectFuelTank(Tank* tank);
	bool disconnectFuelTank(Tank* tank);
	void listConnectedFuelTanks();
	bool startEngine();
	bool stopEngine();
	double getTotalFuelQuantity() const;
	double getConsumedFuelQuantity() const;
	bool getStatus() const;
};
