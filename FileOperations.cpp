#include "FileOperations.h"

// Singleton Pattern
FileOperations* FileOperations::instance = 0;
string FileOperations::inputFileName = "";
string FileOperations::outputFileName = "";
FileOperations* FileOperations::getInstance(string inputFileName, string outputFileName) {
	if (!instance) {
		instance = new FileOperations(inputFileName, outputFileName);
	}
	return instance;
}

FileOperations::FileOperations(string inputFileName, string outputFileName) {
	this->inputFileName = inputFileName;
	this->outputFileName = outputFileName;
}

/*
* This functions reads the input file, finds commands and returns arrays of commands.
*/
vector<string>* FileOperations::inputFile() {
	if (inputFileName.length() != 0) {
		fstream File;
		File.open(inputFileName, ios::in);
		bool isOpen = File.is_open();
		if (isOpen) {
			vector<string>* commandList = new vector<string>();
			string Line;
			while (File.good() && getline(File, Line, ';') && !File.eof()){
				if (Line[0] == '\n') {
					Line = Line.substr(1);
				}
				commandList->push_back(Line);
			}
			File.close();
			return commandList;
		}
		else {
			throw invalid_argument("Error: File can not open.");
		}
	}
	else {
		throw invalid_argument("Error: File does not exist.");
	}
}


/*
* This functions writes logs to output file.
* @param string logMessage
*/
void FileOperations::outputFile(string logMessage) {
	if (outputFileName.length() != 0 && logMessage.length() != 0) {
		fstream File;
		File.open(outputFileName, ios::app);

		tm now;
		time_t currentTime = time(0);
		localtime_s(&now, &currentTime);

		File << "[" << now.tm_mday << "." << (now.tm_mon + 1) << "." << (now.tm_year + 1900) << "][" << now.tm_hour << ":" << now.tm_min << ":" << now.tm_sec << "]" << (logMessage + "\n");
		File.close();
	}
}