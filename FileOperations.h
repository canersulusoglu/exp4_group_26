#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <ctime>
using namespace std;

class FileOperations {
private:
	static FileOperations* instance;
	FileOperations(string, string);
	static string inputFileName;
	static string outputFileName;
public:
	static FileOperations* getInstance(string, string);
	static vector<string>* inputFile();
	static void outputFile(string);
};