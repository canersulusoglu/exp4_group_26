#pragma once
#include <list>
using namespace std;

class Observer {
public:
	virtual void update() = 0;
	virtual int getId() = 0;
};

class Subject {
private:
	list<Observer*>* Observers;
public:
	Subject() { Observers = new list<Observer*>(); };

	void attach(Observer* observer) {
		Observers->push_back(observer);
	}

	void detach(Observer* observer) {
		Observers->remove(observer);
	}

	Observer* getObserver(int id) {
		for (auto it = Observers->begin(); it != Observers->end(); it++) {
			if ((*it)->getId() == id) {
				return (*it);
			}
		}
		return NULL;
	}

	void notify() {
		for (auto it = Observers->begin(); it != Observers->end(); it++) {
			(*it)->update();
		}
	}

	virtual void setState() = 0;
	virtual bool getState() = 0;
};