#pragma once
#include<iostream>
#include<string>
#include <vector>
#include<chrono>
#include<thread>
#include "Engine.h"
#include "Tank.h"
#include "FileOperations.h"
#include "ObserverPattern.h"
#include "SimulationStoppedSubject.h"
#include "SimulationStoppedObserver.h"
using namespace std;

class Simulation {
private:
	vector<string>* commands;
	Engine* engine;
	vector<Tank*>* fuelTanks;
	// Observer Pattern Subject
	Subject* engineSimulationStoppedSubject;
	Subject* tankSimulationStoppedSubject;
	Subject* valveSimulationStoppedSubject;

	void listFuelTanks();
	void printFuelTankCount();
	bool printTankInfo(int);
	bool addFuelTank(double);
	bool removeFuelTank(int);
	bool fillTank(int, double);
	bool breakFuelTank(int);
	bool repairFuelTank(int);
	bool openValve(int);
	bool closeValve(int);
	bool connectFuelTankToEngine(int);
	bool disconnectFuelTankFromEngine(int);
	void listConnectedFuelTanks();
	void printTotalFuelQuantity();
	void printTotalConsumedFuelQuantity();
public:
	Simulation(vector<string>*);
	~Simulation();
	void startSimulation();
};