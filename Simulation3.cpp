#include <stdio.h>
#include <iostream>
#include "Simulation.h"

using namespace std;


/*
* This function opens the valve of a tank by its ID
* @param int tankId
*/
bool Simulation::openValve(int tankId)
{
	bool tankExists = false;
	bool valveStatus = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->openValve();
				valveStatus = (*i)->getValveStatus();
				break;
			}
		}
	}

	return (tankExists && valveStatus);
}
/*
* This function closes the valve of a tank by its ID
* @param int tankId
*/
bool Simulation::closeValve(int tankId)
{
	bool tankExists = false;
	bool valveStatus = true;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->closeValve();
				valveStatus = (*i)->getValveStatus();
				break;
			}
		}
	}

	return (tankExists && !valveStatus);
}
/*
* This function breaks a fuel tank by its ID
* @param int tankId
*/
bool Simulation::breakFuelTank(int tankId)
{
	bool tankExists = false;
	bool isBroken = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->breakTank();
				isBroken = (*i)->isItBroken();
				break;
			}
		}
	}

	return (tankExists && isBroken);
}
/*
* This function repairs a fuel tank by its ID
* @param int tankId
*/
bool Simulation::repairFuelTank(int tankId)
{
	bool tankExists = false;
	bool isBroken = true;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->repairTank();
				isBroken = (*i)->isItBroken();
				break;
			}
		}
	}

	return (tankExists && !isBroken);
}

/*
* This function prints the total fuel tank count
*/
void Simulation::printFuelTankCount()
{
	cout << "Fuel Tank Count: " << this->fuelTanks->size() << endl;
}

/*
* This function prints a tanks information by its ID using the Tank class
* Returns the status of the function ends by a success or not
* @param int tankId
*/
bool Simulation::printTankInfo(int tankId)
{
	bool tankExists = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->printInfo();
				break;
			}
		}
	}

	return tankExists;
}

/*
* This function prints a list of fuel tanks by using the Engine class
*/
void Simulation::listConnectedFuelTanks()
{
	this->engine->listConnectedFuelTanks();

}