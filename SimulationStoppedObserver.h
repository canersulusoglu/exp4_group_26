#pragma once
#include "ObserverPattern.h"
#include <iostream>
using namespace std;

class EngineSimulationStoppedObserver : public Observer {
public:
	void update() {
		cout << "Engine: Simulation stopped." << endl;
	}
	int getId() {
		return 0;
	}
};

class TankSimulationStoppedObserver : public Observer {
private:
	int id;
public:
	TankSimulationStoppedObserver(int id) { this->id = id; }
	void update() {
		cout << "Tank " << id << ": Simulation stopped." << endl;
	}
	int getId() {
		return this->id;
	}
};

class ValveSimulationStoppedObserver : public Observer {
private:
	int id;
public:
	ValveSimulationStoppedObserver(int id) { this->id = id; }
	void update() {
		cout << "Valve " << id << ": Simulation stopped." << endl;
	}
	int getId() {
		return this->id;
	}
};