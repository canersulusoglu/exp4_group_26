#pragma once
#include "ObserverPattern.h"

class EngineSimulationStoppedSubject : public Subject {
private:
	bool state;
public:
	EngineSimulationStoppedSubject() { state = false; }

	void setState() {
		state = !state;

		this->notify();
	}

	bool getState() {
		return state;
	}
};

class TankSimulationStoppedSubject : public Subject {
private:
	bool state;
public:
	TankSimulationStoppedSubject() { state = false; }

	void setState() {
		state = !state;

		this->notify();
	}

	bool getState() {
		return state;
	}
};

class ValveSimulationStoppedSubject : public Subject {
private:
	bool state;
public:
	ValveSimulationStoppedSubject() { state = false; }

	void setState() {
		state = !state;

		this->notify();
	}

	bool getState() {
		return state;
	}
};
