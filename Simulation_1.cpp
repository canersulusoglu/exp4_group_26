#include "Simulation.h"
using namespace std;

/*
* Constructor
* @param vector<string>* commands
*/
Simulation::Simulation(vector<string>* commands){
	this->engine = Engine::getInstance();
	this->fuelTanks = new vector<Tank*>();
	this->commands = commands;
	// Observer Pattern Subjects
	engineSimulationStoppedSubject = new EngineSimulationStoppedSubject();
	tankSimulationStoppedSubject = new TankSimulationStoppedSubject();
	valveSimulationStoppedSubject = new ValveSimulationStoppedSubject();
	// Engine add Observer
	Observer* engineSimulationStoppedObserver = new EngineSimulationStoppedObserver();
	engineSimulationStoppedSubject->attach(engineSimulationStoppedObserver);
}

/*
* Destructor
*/
Simulation::~Simulation(){
	delete this->engine;
	delete this->fuelTanks;
	delete this->commands;
	delete this->engineSimulationStoppedSubject;
	delete this->tankSimulationStoppedSubject;
	delete this->valveSimulationStoppedSubject;
}


/*
* This function checks the commands and run these commands.
*/
void Simulation::startSimulation() {
	FileOperations::outputFile("Simulation started.");

	for (int i = 0; i < this->commands->size(); i++) {
		string Command = this->commands->at(i);
		string CommandName;
		vector<double> Parameters;

		string const delimiters{ " " };
		size_t beg, pos = 0;
		bool isName = true;
		while ((beg = Command.find_first_not_of(delimiters, pos)) != string::npos) {
			pos = Command.find_first_of(delimiters, beg + 1);
			string parsed = Command.substr(beg, pos - beg);
			if (isName) {
				CommandName = parsed;
				isName = false;
			}
			else {
				Parameters.push_back(stod(parsed));
			}
		}

		if (CommandName == "start_engine") {
			bool result = this->engine->startEngine();
			if (result)
				FileOperations::outputFile("Engine has been started.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "stop_engine") {
			bool result = this->engine->stopEngine();
			if (result)
				FileOperations::outputFile("Engine has been stopped.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "add_fuel_tank") {
			bool result = this->addFuelTank(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Added a new fuel tank which has " + to_string(Parameters.at(0)) + " quantity.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "remove_fuel_tank") {
			bool result = this->removeFuelTank(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Removed a fuel tank which has " + to_string(Parameters.at(0)) + " id.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "fill_tank") {
			bool result = this->fillTank(Parameters.at(0), Parameters.at(1));
			if (result)
				FileOperations::outputFile(to_string(Parameters.at(1)) + " fuel was filled in the tank with " + to_string(Parameters.at(0)) + " id.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "break_fuel_tank") {
			bool result = this->breakFuelTank(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Breaked a fuel tank which has " + to_string(Parameters.at(0)) + " id.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "repair_fuel_tank") {
			bool result = this->repairFuelTank(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Repaired a fuel tank which has " + to_string(Parameters.at(0)) + " id.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "connect_fuel_tank_to_engine") {
			bool result = this->connectFuelTankToEngine(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Tank number " + to_string(Parameters.at(0)) + " was connected to the engine.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "disconnect_fuel_tank_from_engine") {
			bool result = this->disconnectFuelTankFromEngine(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Tank number " + to_string(Parameters.at(0)) + " was disconnected from the engine.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "give_back_fuel") {
			bool result = this->engine->giveBackFuel(Parameters.at(0));
			if (result)
				FileOperations::outputFile(to_string(Parameters.at(0)) + " fuel was taken from the engine");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "open_valve") {
			bool result = this->openValve(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Tank number " + to_string(Parameters.at(0)) + " valve opened.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "close_valve") {
			bool result = this->closeValve(Parameters.at(0));
			if (result)
				FileOperations::outputFile("Tank number " + to_string(Parameters.at(0)) + " valve closed.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "list_fuel_tanks") {
			this->listFuelTanks();
			FileOperations::outputFile("Listed fuel tanks.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "print_fuel_tank_count") {
			this->printFuelTankCount();
			FileOperations::outputFile("Printed fuel tank count.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "list_connected_tanks") {
			this->listConnectedFuelTanks();
			FileOperations::outputFile("Listed connected fuel tanks.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "print_total_fuel_quantity") {
			this->printTotalFuelQuantity();
			FileOperations::outputFile("Printed total fuel quantitiy.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "print_total_consumed_fuel_quantity") {
			this->printTotalConsumedFuelQuantity();
			FileOperations::outputFile("Printed total consumed fuel quantitiy.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "print_tank_info") {
			bool result = this->printTankInfo(Parameters.at(0));
			if (result)
				FileOperations::outputFile("The information of the tank with " + to_string(Parameters.at(0)) + " id was printed.");
			this_thread::sleep_for(chrono::seconds(1));
			this->engine->absorbFuel();
		}
		else if (CommandName == "wait") {
			this_thread::sleep_for(chrono::seconds((long long)Parameters.at(0)));
			FileOperations::outputFile("Waited " + to_string(Parameters.at(0)) + " second(s).");
			for (int i = 0; i < Parameters.at(0); i++) {
				this->engine->absorbFuel();
			}
		}
		else if (CommandName == "stop_simulation") {
			FileOperations::outputFile("Simulation stopped.");
			engineSimulationStoppedSubject->setState();
			tankSimulationStoppedSubject->setState();
			valveSimulationStoppedSubject->setState();
			break;
		}
		else {
			throw invalid_argument("There is no such command.(" + CommandName + ")");
		}
	}
}