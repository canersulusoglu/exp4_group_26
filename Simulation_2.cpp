#include <stdio.h>
#include <iostream>
#include "Simulation.h"

using namespace std;

/*
* This function shows the existing fuel tanks.
*/
void Simulation::listFuelTanks()   
{
	cout << "---------  List Fuel Tanks ---------" << endl;
	for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
	{
		(*i)->printInfo();
	}
	cout << "------------------------------------" << endl;
}
/*
* This function adds a fuel tank.
* @param double capacity
*/
bool Simulation::addFuelTank(double capacity)             
{
	if (capacity > 0)
	{
		if (this->fuelTanks->size() == 0)
		{
			Tank* newFuelTank = new Tank(1, capacity);
			this->fuelTanks->push_back(newFuelTank);
			// Observer Tank add.
			Observer* tankSimulationStoppedObserver = new TankSimulationStoppedObserver(1);
			tankSimulationStoppedSubject->attach(tankSimulationStoppedObserver);
			// Observer Valve add.
			Observer* valveSimulationStoppedObserver = new ValveSimulationStoppedObserver(1);
			valveSimulationStoppedSubject->attach(valveSimulationStoppedObserver);
		}
		else
		{
			int lastTankId = this->fuelTanks->back()->getId();

			Tank* newFuelTank = new Tank((lastTankId + 1), capacity);
			this->fuelTanks->push_back(newFuelTank);

			// Observer Tank add.
			Observer* tankSimulationStoppedObserver = new TankSimulationStoppedObserver((lastTankId + 1));
			tankSimulationStoppedSubject->attach(tankSimulationStoppedObserver);
			// Observer Valve add.
			Observer* valveSimulationStoppedObserver = new ValveSimulationStoppedObserver((lastTankId + 1));
			valveSimulationStoppedSubject->attach(valveSimulationStoppedObserver);
		}

		return true;
	}

	return false;
}
/*
* This function removes a fuel tank
* @param int tankId
*/
bool Simulation::removeFuelTank(int tankId)   
{
	bool tankExists = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				this->fuelTanks->erase(i);
				// Observer Tank remove.
				Observer* tankSimulationStoppedObserver = tankSimulationStoppedSubject->getObserver(fuelTankId);
				if(tankSimulationStoppedObserver != NULL)
					tankSimulationStoppedSubject->detach(tankSimulationStoppedObserver);
				// Observer Valve remove.
				Observer* valveSimulationStoppedObserver = valveSimulationStoppedSubject->getObserver(fuelTankId);
				if (valveSimulationStoppedObserver != NULL)
					valveSimulationStoppedSubject->detach(valveSimulationStoppedObserver);
				break;
			}
		}
	}

	return tankExists;
}
/*
* This function connects a fuel tank to the engine
* @param int tankId
*/
bool Simulation::connectFuelTankToEngine(int tankId)   
{
	bool tankExists = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();
			if (tankId == fuelTankId)
			{
				tankExists = true;
				this->engine->connectFuelTank((*i));
				break;
			}
		}
	}

	return tankExists;
}
/*
* This function disconnets a fuel tank from the engine
* @param int tankId
*/
bool Simulation::disconnectFuelTankFromEngine(int tankId)
{
	bool tankExists = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();            
			if (tankId == fuelTankId)
			{
				tankExists = true;
				this->engine->disconnectFuelTank((*i));     
				break;
			}
		}
	}

	return tankExists;
}
/*
* This function fills the given tank
* @param int tankId
*/
bool Simulation::fillTank(int tankId, double quantity)
{
	bool tankExists = false;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
		{
			int fuelTankId = (*i)->getId();            
			if (tankId == fuelTankId)
			{
				tankExists = true;
				(*i)->fillTank(quantity); 
				break;
			}
		}
	}

	return tankExists;
}
/*
* This function prints the total fuel quantity existent in the tanks
* @param int tankId
*/
void Simulation::printTotalFuelQuantity()
{
	double totalFuelQuantity = 0;

	if (!this->fuelTanks->empty())
	{
		for (auto i = this->fuelTanks->begin(); i != this->fuelTanks->end(); ++i)
			totalFuelQuantity += (*i)->getFuelQuantity();
	}

	cout << "Total Fuel Quantity: " << totalFuelQuantity << endl;
}
/*
* This function prints the total consumed fuel quantity from the tanks
* @param int tankId
*/
void Simulation::printTotalConsumedFuelQuantity()
{
	double totalConsumedFuelQuantity = this->engine->getConsumedFuelQuantity();

	cout << "Total ConsumedFuel Quantity: " << totalConsumedFuelQuantity << endl;
}
