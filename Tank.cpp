#include "Tank.h"
#include <iostream>

using namespace std;

Tank::Tank(int id, double capacity) {
	this->id = id;
	this->capacity = capacity;
	this->broken = false;
	this->fuelQuantity = capacity;
}

Tank::Tank() {
	this->broken = false;
	this->fuelQuantity = 0;
}

void Tank::breakTank() {
	this->broken = true;
}

void Tank::repairTank() {
	this->broken = false;
}

void Tank::openValve() {
	this->valve.open();
}

void Tank::closeValve() {
	this->valve.close();
}

bool Tank::getValveStatus() const {
	return this->valve.getStatus();
}

bool Tank::isItBroken() const {
	return this->broken;
}

int Tank::getId() const {
	return this->id;
}

double Tank::getFuelQuantity() const {
	return this->fuelQuantity;
}

void Tank::printInfo() const {
	cout << "Id: " << id << endl << "Capacity: " << capacity << endl;
	cout << "Fuel Quantity: " << fuelQuantity << endl << "Status: " << (broken ? "Broken" : "Not broken") << endl;
}

void Tank::fillTank(double quantity){
	
	bool valveIsOpen = this->valve.getStatus();

	if (valveIsOpen) {
		if (quantity + this->fuelQuantity < this->capacity)
			this->fuelQuantity += quantity;

		else
			cout << "Fuel quantity must be smaller than tank's capacity!" << endl;
	}
	else
		cout << "Valve must be opened to fill the tank!" << endl;
	
}

void Tank::absorbFuel(double quantity) {
	double newQuantity = this->fuelQuantity - quantity;
	if (newQuantity < 0) {
		this->fuelQuantity = 0;
	}
	else {
		this->fuelQuantity = newQuantity;
	}
}

double Tank::getCapacity() const {
	return this->capacity;
}