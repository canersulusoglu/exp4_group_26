#pragma once
#include "Valve.h"

class Tank {
private:
	int id;
	double capacity;
	double fuelQuantity;
	bool broken;
	Valve valve;
public:
	Tank(int, double);
	Tank();
	void breakTank();
	void repairTank();
	void openValve();
	void closeValve();
	bool getValveStatus() const;
	bool isItBroken() const;
	void printInfo() const;
	void fillTank(double quantity);   // setFuelQuantity
	int getId() const;
	double getFuelQuantity() const;
	void absorbFuel(double);
	double getCapacity() const;

};
	