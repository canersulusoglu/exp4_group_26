#include <iostream>
#include "Valve.h"

/*
* This is the default constructor for the Valve class.
*/
Valve::Valve() {
	this->status = false;
}

/*
* This function opens the valve.
*/
void Valve::open() { this->status = true; }

/*
* This function closes the valve.
*/
void Valve::close() { this->status = false; }

/*
* This function returns the status of the Valve.
*/
bool Valve::getStatus() const { return this->status; }