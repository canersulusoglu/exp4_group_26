#pragma once

class Valve {

private:
	bool status;

public:
	Valve();
	void open();
	void close();
	bool getStatus() const;
};