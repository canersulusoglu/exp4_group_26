#include <iostream>
#include "FileOperations.h"
#include "Simulation.h"

/*
* These lines of codes provide to read the file name from the command line with the help of "char* argv[1]".
* The s string store the name of the file.
* The s string send as a parameter to the inputFile function in FileOperations class.
* Returning value from the input file, stores in the commands pointer.
* The "commands" pointer send to the Simulation constructor.
* Not entered file names and empty file names are checked with try-catch method.
*
* @param int argc
* @param char* argv[]
*/
int main(int argc, char* argv[])
{
	try
	{
		if (argc < 3){
			throw invalid_argument("Wrong Format! Correct format is: './app.exe input.txt output.txt'");
		}
		string inputfilename = argv[1];
		string outputfilename = argv[2];

		FileOperations::getInstance(inputfilename, outputfilename);
		vector<string>* commands = FileOperations::inputFile();

		if (commands->size() == 0) {
			throw invalid_argument("There is no command in the file.");
		}

		Simulation* simulation = new Simulation(commands);
		simulation->startSimulation();
		delete simulation;
	}
	catch (const invalid_argument& e)
	{
		cerr << e.what() << endl;
		return -1;
	}
	return 0;
}
